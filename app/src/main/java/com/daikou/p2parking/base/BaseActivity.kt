package com.daikou.p2parking.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import com.daikou.p2parking.R
import com.daikou.p2parking.helper.AuthHelper
import com.daikou.p2parking.helper.HelperUtil
import com.daikou.p2parking.helper.LoadingDialog
import com.daikou.p2parking.helper.SunmiPrintHelper
import com.daikou.p2parking.model.Constants
import com.sunmi.pay.hardware.aidlv2.AidlErrorCodeV2
import com.sunmi.pay.hardware.aidlv2.emv.EMVOptV2
import com.sunmi.pay.hardware.aidlv2.etc.ETCOptV2
import com.sunmi.pay.hardware.aidlv2.pinpad.PinPadOptV2
import com.sunmi.pay.hardware.aidlv2.print.PrinterOptV2
import com.sunmi.pay.hardware.aidlv2.readcard.ReadCardOptV2
import com.sunmi.pay.hardware.aidlv2.security.SecurityOptV2
import com.sunmi.pay.hardware.aidlv2.system.BasicOptV2
import com.sunmi.pay.hardware.aidlv2.tax.TaxOptV2
import sunmi.paylib.SunmiPayKernel
import sunmi.paylib.SunmiPayKernel.ConnectCallback

open class BaseActivity : BaseCoreActivity() {


    /* Mifare Card*/
    var basicOptV2: BasicOptV2? = null // 获取基础操作模块

    var readCardOptV2: ReadCardOptV2? = null // 获取读卡模块

    var pinPadOptV2: PinPadOptV2? = null // 获取PinPad操作模块

    var securityOptV2: SecurityOptV2? = null // 获取安全操作模块

    var emvOptV2: EMVOptV2? = null // 获取EMV操作模块

    var taxOptV2: TaxOptV2? = null // 获取税控操作模块

    var etcOptV2: ETCOptV2? = null // 获取ETC操作模块

    var printerOptV2: PrinterOptV2? = null // 获取打印操作模块

    private var connectPaySDK = false //是否已连接PaySDK
    private var connectPaySDKListener: ConnectPaySDKListener? =
        null

    public fun setConnectPaySDKListener(connectPaySDKListener: ConnectPaySDKListener) {
        this.connectPaySDKListener = connectPaySDKListener
    }


    /* Mifare Card*/


    private var timeMap: HashMap<String, Long> = HashMap()
    private var loadDialog: LoadingDialog? = null

    private lateinit var handler: Handler
    var mLanguage: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handler = Handler(Looper.getMainLooper())

        initPrinter()

        mLanguage = HelperUtil.getStringSharePreference(this, Constants.Auth.LANGUAGE)
        if (mLanguage == "") {
            mLanguage = "en"
        }
        HelperUtil.changeLanguage(this, mLanguage ?: "en")

    }

    private fun initPrinter() {
        SunmiPrintHelper.getInstance().initPrinter()
    }

    open fun gotoActivity(activity: Activity, intent: Intent) {
        activity.startActivity(intent)
    }

    open fun gotoActivityForResult(
        activity: Activity,
        intent: Intent,
        activityResult: BetterActivityResult.OnActivityResult<ActivityResult>
    ) {
        if (activity is BaseActivity) {
            activity.activityLauncher.launch(intent, activityResult)
        }
    }

    open fun initPrinterService() {
        when (SunmiPrintHelper.getInstance().sunmiPrinter) {
            SunmiPrintHelper.NoSunmiPrinter -> {
                Log.d("no printer", "No printer")
            }

            SunmiPrintHelper.CheckSunmiPrinter -> {
                handler.postDelayed(
                    {
                        initPrinterService()
                    }, 2000
                )
            }

            SunmiPrintHelper.FoundSunmiPrinter -> {
                Log.d("connecting", "Connecting to printer")
            }

            else -> {
                SunmiPrintHelper.getInstance().initSunmiPrinterService(this)
            }
        }
    }

    open fun isLogin(mActivity: Activity): Boolean {
        return AuthHelper.getAccessToken(mActivity) != ""
    }


    /*P2 Mifare Card*/

    open fun showToast(redId: Int) {
        showToastOnUI(getString(redId))
    }


    open fun showToast(msg: String) {
        showToastOnUI(msg)
    }

    open fun showToastOnUI(msg: String) {
        runOnUiThread { Toast.makeText(this, msg, Toast.LENGTH_SHORT).show() }
    }

    open fun toastHint(code: Int) {
        if (code == 0) {
            showToast(R.string.success)
        } else {
            val msg = AidlErrorCodeV2.valueOf(code).msg
            val error = "$msg:$code"
            showToast(error)
        }
    }

    protected open fun showLoadingDialog(resId: Int) {
        runOnUiThread { _showLoadingDialog(getString(resId)) }
    }

    protected open fun showLoadingDialog(msg: String) {
        runOnUiThread { _showLoadingDialog(msg) }
    }

    /** This method should be called in UI thread  */
    open fun _showLoadingDialog(msg: String) {
        if (loadDialog == null) {
            loadDialog = LoadingDialog(this, msg)
        } else {
            loadDialog?.setMessage(msg)
        }
        if (!loadDialog?.isShowing!!) {
            loadDialog?.show()
        }
    }

    protected open fun openActivity(clazz: Class<out Activity?>?) {
        val intent = Intent(this, clazz)
        openActivity(intent, false)
    }

    protected open fun openActivity(clazz: Class<out Activity?>?, finishSelf: Boolean) {
        val intent = Intent(this, clazz)
        openActivity(intent, finishSelf)
    }

    protected open fun openActivity(intent: Intent?, finishSelf: Boolean) {
        startActivity(intent)
        if (finishSelf) {
            finish()
        }
    }

    protected open fun openActivityForResult(clazz: Class<out Activity?>?, requestCode: Int) {
        val intent = Intent(this, clazz)
        openActivityForResult(intent, requestCode)
    }

    protected open fun openActivityForResult(intent: Intent?, requestCode: Int) {
        startActivityForResult(intent!!, requestCode)
    }


    protected open fun addStartTime(key: String) {
        timeMap.put("start_$key", SystemClock.elapsedRealtime())
    }

    protected open fun addStartTimeWithClear(key: String) {
        timeMap.clear()
        timeMap.put("start_$key", SystemClock.elapsedRealtime())
    }

    protected open fun addEndTime(key: String) {
        timeMap.put("end_$key", SystemClock.elapsedRealtime())
    }

    protected open fun showSpendTime() {
        var startValue: Long? = null
        var endValue: Long? = null
        for (key in timeMap.keys) {
            if (!key.startsWith("start_")) {
                continue
            }
            val key = key.substring("start_".length)
            startValue = timeMap.get("start_$key")
            endValue = timeMap.get("end_$key")
            if (startValue == null || endValue == null) {
                continue
            }
        }
    }


    /** bind PaySDK service  */
    open fun bindPaySDKService() {
        val payKernel = SunmiPayKernel.getInstance()
        payKernel.initPaySDK(this, object : ConnectCallback {
            override fun onConnectPaySDK() {
                emvOptV2 = payKernel.mEMVOptV2
                basicOptV2 = payKernel.mBasicOptV2
                pinPadOptV2 = payKernel.mPinPadOptV2
                readCardOptV2 = payKernel.mReadCardOptV2
                securityOptV2 = payKernel.mSecurityOptV2
                taxOptV2 = payKernel.mTaxOptV2
                etcOptV2 = payKernel.mETCOptV2
                printerOptV2 = payKernel.mPrinterOptV2
                connectPaySDK = true
                Log.d("ONMIFARE", "onConnectPaySDK")
                connectPaySDKListener?.onConnectPaySDK()

            }

            override fun onDisconnectPaySDK() {
                connectPaySDK = false
                emvOptV2 = null
                basicOptV2 = null
                pinPadOptV2 = null
                readCardOptV2 = null
                securityOptV2 = null
                taxOptV2 = null
                etcOptV2 = null
                printerOptV2 = null
                Log.d("ONMIFARE", "onDisconnectPaySDK")
            }
        })
    }


    interface ConnectPaySDKListener {
        fun onConnectPaySDK()
    }

}